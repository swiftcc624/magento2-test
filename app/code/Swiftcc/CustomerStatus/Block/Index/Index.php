<?php

namespace Swiftcc\CustomerStatus\Block\Index;


use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\View\Element\Template\Context;

class Index extends \Magento\Framework\View\Element\Template {
    protected $customerRepository;
    protected $customerSession;

    public function __construct(
        Context $context,
        Session $customerSession,
        CustomerRepositoryInterface $customerRepository
    ) {
        parent::__construct($context);
        $this->customerSession = $customerSession;
        $this->customerRepository = $customerRepository;
    }

    protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    public function getCustomerStatus()
    {
        $customerId = $this->customerSession->getCustomer()->getId();
        if ($customerId) {
            $customerDataObject = $this->customerRepository->getById($customerId);
            $customer_status = $customerDataObject->getCustomAttribute('customer_status');
            return $customer_status->getValue();
        }
        return "";
    }

    public function getPostActionUrl()
    {
        return $this->getUrl('customerstatus/index/post');
    }
}