<?php

namespace Swiftcc\CustomerStatus\Block\Index;

use Magento\Customer\Block\Account\SortLinkInterface;

class Link extends \Magento\Framework\View\Element\Html\Link implements SortLinkInterface
{
    /**
     * Render block HTML.
     *
     * @return string
     */
    protected function _toHtml()
    {
        if (false != $this->getTemplate()) {
            return parent::_toHtml();
        }
        return '<li><a ' . $this->getLinkAttributes() . ' >' . $this->escapeHtml($this->getLabel()) . '</a></li>';
    }

    public function getHref()
    {
        return $this->getUrl('customerstatus/index/index');
    }

    public function getSortOrder()
    {
        return $this->getData(self::SORT_ORDER);
    }
}