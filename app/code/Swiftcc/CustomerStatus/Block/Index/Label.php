<?php

namespace Swiftcc\CustomerStatus\Block\Index;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Block\Account\SortLinkInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\View\Element\Template\Context;

class Label extends \Magento\Framework\View\Element\Html\Link implements SortLinkInterface
{
    protected $customerRepository;
    protected $customerSession;

    public function __construct(
        Context $context,
        Session $customerSession,
        CustomerRepositoryInterface $customerRepository
    ) {
        parent::__construct($context);
        $this->customerSession = $customerSession;
        $this->customerRepository = $customerRepository;
    }

    protected function _toHtml()
    {
        if (false != $this->getTemplate()) {
            return parent::_toHtml();
        }

        $customerId = $this->customerSession->getCustomer()->getId();
        if ($customerId) {
            $customerDataObject = $this->customerRepository->getById($customerId);
            $customer_status = $customerDataObject->getCustomAttribute('customer_status');
            $customer_status_value = $customer_status->getValue();
            return "<li>{$customer_status_value}</li>/";
        }
        return "";
    }

    public function getHref()
    {
        return $this->getUrl('customerstatus/index/index');
    }

    public function getSortOrder()
    {
        return $this->getData(self::SORT_ORDER);
    }
}