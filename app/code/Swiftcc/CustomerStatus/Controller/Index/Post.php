<?php

namespace Swiftcc\CustomerStatus\Controller\Index;

use Magento\Customer\Model\Customer;
use Magento\Customer\Model\ResourceModel\CustomerFactory as CustomerResourceFactory;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\App\Action\Context;

class Post extends \Magento\Framework\App\Action\Action
{
    const CUSTOM_CUSTOMER_ATTR = 'customer_status';
    protected $customerResourceFactory;
    protected $customerModel;
    protected $customerSession;

    public function __construct(
        Context $context,
        CustomerResourceFactory $customerResourceFactory,
        Customer $customerModel,
        CustomerSession $customerSession
    ) {
        parent::__construct($context);
        $this->customerResourceFactory = $customerResourceFactory;
        $this->customerModel = $customerModel;
        $this->customerSession = $customerSession;
    }


    public function execute()
    {
        if ($this->customerSession->isLoggedIn()) {
            $post = $this->getRequest()->getPostValue();
            if (!$post) {
                return $this->_redirect('*/*/');
            }

            $customAttributeValue = $post['customer_status'];

            $customerId = $this->customerSession->getCustomer()->getId();

            $customerNew = $this->customerModel->load($customerId);
            $customerData = $customerNew->getDataModel();
            $customerData->setCustomAttribute(self::CUSTOM_CUSTOMER_ATTR, $customAttributeValue);
            $customerNew->updateData($customerData);

            $customerResource = $this->customerResourceFactory->create();
            $customerResource->saveAttribute($customerNew, self::CUSTOM_CUSTOMER_ATTR);

            $this->messageManager->addSuccess(__('You are successfully save the data!!'));
        }

        return $this->_redirect('*/*/');
    }
}
